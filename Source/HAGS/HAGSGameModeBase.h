// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HAGSGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HAGS_API AHAGSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
