// Copyright Epic Games, Inc. All Rights Reserved.

#include "HAGS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HAGS, "HAGS" );
